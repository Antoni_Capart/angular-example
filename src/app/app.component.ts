import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  animals = [
    {
      name: 'Girafe',
      image: 'girafe.jpg'
    },
    {
      name: 'Lion',
      image: 'lion.jpg'
    },
    {
      name: 'Chat',
      image: 'chat.jpg'
    },
  ];

  animalShow = {
    name: '',
    image: '',
  };

  eventAnimal = (animal) => {
    this.animalShow = animal;
  }
}
